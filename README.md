# cruxports

My Collection of Ports for CRUX Linux

### Installing and using it

To use my ports with prt-get, execute following commands:

	cd /usr/ports

	git clone http://codeberg.org/glowiak/cruxports.git

then add to /etc/prt-get.conf:

	prtdir /usr/ports/cruxports

also you have to change in /etc/pkgmk.conf, from this:

	# PKGMK_IGNORE_FOOTPRINT="yes"

to

	PKGMK_IGNORE_FOOTPRINT="yes"

### Updating local tree

To update local tree execute following command:

	cd /usr/ports && rm -rf /usr/ports/cruxports && git clone http://codeberg.org/glowiak/cruxports.git

### prt-get repos

If you're using cruxports with 
	prt-get
 then you should enable 
	opt
 and
	contrib
 repositories, which contains Ports' dependiences.

### harfbuzz

This Ports repository contains harfbuzz package. I know that this package is also in opt repo, but the opt one is broken. My package works. If want to install my harfbuzz port with prt-get, move 'prtdir /usr/ports/opt' below 'prtdir /usr/ports/cruxports'
